﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthbar;

    private int startHp = 100;
    public int hp;

    private int deathOrder = 0;
    private int playersRemain = 2;

    public enum RaiseEventsCode
    {
        LastAlive = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    private void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.LastAlive)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            deathOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfDeadPlayer + " " + deathOrder);

            GameObject orderUiText = RacingGameManager.instance.finisherTextsUi[deathOrder - 1];
            orderUiText.SetActive(true);


            if (viewId == photonView.ViewID)//you
            {
                orderUiText.GetComponent<Text>().text = deathOrder + " " + nickNameOfDeadPlayer + "(YOU)" + "WON" ;
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUiText.GetComponent<Text>().text = deathOrder + " " + nickNameOfDeadPlayer;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        hp = startHp;
        healthbar.fillAmount = hp / startHp;
    }

    [PunRPC]
    public void TakeDamage(int damage)
    {
        hp -= damage;
        Debug.Log(hp);
        healthbar.fillAmount = hp / startHp;

        if (hp < 0)
        {
            Die();

            if(playersRemain == 1)
            {
                GameObject leaderboard = RacingGameManager.instance.finisherTextsUi[deathOrder + 1];
                leaderboard.SetActive(true);
            }
        }
    }

    private void Die()
    {
        if(photonView.IsMine)
        {
            RacingGameManager.instance.LeaveRoom();
        }

        deathOrder++;
        playersRemain--;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, deathOrder, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.LastAlive, data, raiseEventOptions, sendOptions);
    }
}
