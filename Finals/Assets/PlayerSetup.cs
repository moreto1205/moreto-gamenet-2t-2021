﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityStandardAssets.Vehicles.Aeroplane;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    [SerializeField]
    TextMeshProUGUI playerNameText;
    // Start is called before the first frame update

    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        playerNameText.text = photonView.Owner.NickName;
        GetComponent<AeroplaneUserControl2Axis>().enabled = photonView.IsMine;
        GetComponent<AeroplaneController>().enabled = photonView.IsMine;
        GetComponent<AeroplaneControlSurfaceAnimator>().enabled = photonView.IsMine;
        GetComponent<LandingGear>().enabled = photonView.IsMine;
        GetComponent<AeroplaneAudio>().enabled = photonView.IsMine;
        //GetComponent<simplePlane>().enabled = photonView.IsMine;
        camera.enabled = photonView.IsMine;

    }

}
