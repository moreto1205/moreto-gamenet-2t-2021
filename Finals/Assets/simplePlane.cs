﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simplePlane : MonoBehaviour
{
    public Rigidbody rb;
    public float Vspeed;
    public float Xspeed;
    public float Zspeed;
    public float CurrSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            rb.AddForce(Vector3.up * Vspeed);
        }

        if (Input.GetKey(KeyCode.E))
        {
            rb.AddForce(Vector3.down * Vspeed);
        }

        float translation = Input.GetAxis("Vertical") * Xspeed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * Zspeed * Time.deltaTime;

        transform.Translate(0, 0, translation);
        CurrSpeed = translation;
        transform.Rotate(0, rotation, 0);
    }
}
