﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;


public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image HealthBar;

    private float startHealth = 100;
    private float health;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        HealthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void TakeDamage(int damage)
    {
        health -= damage;
        Debug.Log(health);

        HealthBar.fillAmount = health / startHealth;
        if (health < 0)
        {
            Die();
        }
    }

    private void Die()
    {
       if(photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
    }
}
